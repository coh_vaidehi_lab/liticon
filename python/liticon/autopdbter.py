
import math

def _determineIfTer( first_res_lines, second_res_lines, distance_threshold = 5.0 ):
    
    first_res_atom_x_coord = None
    first_res_atom_y_coord = None
    first_res_atom_z_coord = None
            
    second_res_atom_x_coord = None
    second_res_atom_y_coord = None
    second_res_atom_z_coord = None
    
    first_res_chain_id = None
    second_res_chain_id = None
     
    for line in first_res_lines:
        atom_name = line[12:16]
        first_res_chain_id = line[21:22]
        if atom_name == " O  ":
            first_res_atom_x_coord = float( line[30:38] )
            first_res_atom_y_coord = float( line[38:46] )
            first_res_atom_z_coord = float( line[46:54] )
            break

    for line in second_res_lines:
        atom_name = line[12:16]
        second_res_chain_id = line[21:22]
        if atom_name == " N  ":
            second_res_atom_x_coord = float( line[30:38] )
            second_res_atom_y_coord = float( line[38:46] )
            second_res_atom_z_coord = float( line[46:54] )
            break
    
    if ( first_res_atom_x_coord is not None ) and ( second_res_atom_x_coord is not None ) and \
        math.sqrt( ( first_res_atom_x_coord - second_res_atom_x_coord ) ** 2.0 + \
                   ( first_res_atom_y_coord - second_res_atom_y_coord ) ** 2.0 + \
                   ( first_res_atom_z_coord - second_res_atom_z_coord ) ** 2.0  ) > distance_threshold:
        return True
    elif ( first_res_chain_id is not None ) and ( second_res_chain_id is not None ) and \
         ( first_res_chain_id != second_res_chain_id ):
        return True
    else:
        return False

    
def autoPdbTer( input_pdb_filename, output_pdb_filename ):
    
    pdb_lines = []
    
    with open( input_pdb_filename, "r" ) as infile:
        previous_chain_id_res_num = ( "", -999999 )
        previous_res_lines = []
        current_res_lines = []
        for line in infile:
            line_type = line[:6].strip()
            if line_type in [ "TER", "" ]:
                continue
            if line_type in [ "ATOM", "HETATM" ]:
                atom_chain_id = line[21:22]
                atom_res_num = int( line[22:26] )
                current_chain_id_res_num = ( atom_chain_id, atom_res_num )
                if current_chain_id_res_num == previous_chain_id_res_num:
                    current_res_lines.append( line )
                else:
                    pdb_lines += previous_res_lines
                    need_ter = _determineIfTer( previous_res_lines, current_res_lines )
                    if need_ter:
                        pdb_lines.append("TER\n")
                    previous_chain_id_res_num = current_chain_id_res_num
                    previous_res_lines = current_res_lines
                    current_res_lines = []
                    current_res_lines.append( line )
            else:
                pdb_lines += previous_res_lines
                pdb_lines += current_res_lines
                previous_res_lines = []
                current_res_lines = []
                pdb_lines.append( line )
        pdb_lines += previous_res_lines
        pdb_lines += current_res_lines
                
    with open( output_pdb_filename, "w" ) as outfile:
        for line in pdb_lines:
            outfile.write( line )
 

if __name__ == '__main__':
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="pdb file to load", required=True)
    parser.add_argument("-o", help="pdb file to save result as", required=True)
    args = parser.parse_args()
    
    input_pdb_filename = args.i
    output_pdb_filename = args.o
    
    autoPdbTer( input_pdb_filename, output_pdb_filename )
    
    