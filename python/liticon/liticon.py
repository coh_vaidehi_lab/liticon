import os
import re
import six
import itertools
import numpy
import scipy
import math
import operator
import tempfile
import copy

import rosetta as Rosetta
import unimol

from sklearn.decomposition import PCA

from unimol.util import autoterpdb
from functools import reduce


def poseFromPdbAutoTer( input_pdb_filename, extra_arg_bool = False ):
    temporary_filename = tempfile.NamedTemporaryFile().name
    print( "TEMPFILE:", temporary_filename )
    autoterpdb.autoTerPdb( input_pdb_filename, temporary_filename )
    return Rosetta.poseFromFile( temporary_filename, extra_arg_bool )

def tuplesFromList(x, size=2):
    it = iter(x)
    return zip(*[it]*size)


def linspaceTransformRange( range_tuple ):
    return numpy.linspace( range_tuple[0], range_tuple[1], math.ceil( 1.0 + ( range_tuple[1] - range_tuple[0] ) / ( range_tuple[2] + 0.0000001 ) ) )


def getRotationMatrix(axis, theta):
    # if numpy.linalg.norm(axis) == 0:
    #     axis = numpy.array( [0.0, 0.0, 1.0] )
    axis = numpy.asarray(axis)
    theta = numpy.asarray(-theta)
    axis = axis/math.sqrt(numpy.dot(axis, axis))
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return numpy.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                       [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                       [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])


def getRandomAxis():
    random_angle = numpy.random.random() * 2.0 * math.pi
    random_z = numpy.random.random() * 2.0 - 1.0
    random_z_squared = random_z * random_z
    random_y = math.sqrt( 1 - random_z_squared ) * math.sin( random_angle )
    random_x = math.sqrt( 1 - random_z_squared ) * math.cos( random_angle )
    return numpy.array( [ random_x, random_y, random_z ] )


def getRandomRotationMatrix():
    random_axis = getRandomAxis()
    random_rotation_amount = numpy.random.random() * 2.0 * math.pi - ( math.pi )
    random_rotation_matrix = getRotationMatrix( random_axis, random_rotation_amount )
    return random_rotation_matrix


def calcPrincipalAxis(coords):
    pca = PCA(n_components=1)
    pca.fit(coords)
    return pca.components_[0]



class TranformationGenerator:
    
    
    def __init__( self,
                  chain_id = None,
                  residue_range = None,
                  tilt_range = None,
                  gyrate_range = None,
                  rotate_range = None,
                  translate_range = None,
                  elevate_range = None,
                  slide_range = None ):
        
        if residue_range is None:
            residue_range = ( 0, 0 )
        if tilt_range is None:
            tilt_range = ( 0.0, 0.0, 0.0 )
        if gyrate_range is None:
            gyrate_range = ( 0.0, 0.0, 0.0 )
        if rotate_range is None:
            rotate_range = ( 0.0, 0.0, 0.0 )
        if translate_range is None:
            translate_range = ( 0.0, 0.0, 0.0 )
        if elevate_range is None:
            elevate_range = ( 0.0, 0.0, 0.0 )
        if slide_range is None:
            slide_range = ( 0.0, 0.0, 0.0 )
        self.chain_id = chain_id
        self.residue_range = tuple( int(x) for x in residue_range )
        self.tilt_range = tuple( float(x) for x in tilt_range )
        self.gyrate_range = tuple( float(x) for x in gyrate_range )
        self.rotate_range = tuple( float(x) for x in rotate_range )
        self.translate_range = tuple( float(x) for x in translate_range )
        self.elevate_range = tuple( float(x) for x in elevate_range )
        self.slide_range = tuple( float(x) for x in slide_range )
        
        self.tilts = linspaceTransformRange( self.tilt_range )
        self.gyrates = linspaceTransformRange( self.gyrate_range )
        self.rotates = linspaceTransformRange( self.rotate_range )
        self.translates = linspaceTransformRange( self.translate_range )
        self.elevates = linspaceTransformRange( self.elevate_range )
        self.slides = linspaceTransformRange( self.slide_range )
        
        self.product = itertools.product( self.tilts,
                                          self.gyrates,
                                          self.rotates,
                                          self.translates,
                                          self.elevates,
                                          self.slides )
    
    
    def getNumTransforms( self ):
        
        return len( self.tilts ) * len( self.gyrates ) * len( self.rotates ) * len( self.translates ) * len( self.elevates ) * len( self.slides )
    
    
    def __str__( self ):
        
        retval =  "chain:            %s\n" % self.chain_id
        retval += "residue range:    %i %i\n" % self.residue_range
        retval += "tilt range:      %7.3f %7.3f %7.3f\n" % self.tilt_range
        retval += "gyrate range:    %7.3f %7.3f %7.3f\n" % self.gyrate_range
        retval += "rotate range:    %7.3f %7.3f %7.3f\n" % self.rotate_range
        retval += "translate range: %7.3f %7.3f %7.3f\n" % self.translate_range
        retval += "elevate range:   %7.3f %7.3f %7.3f\n" % self.elevate_range
        retval += "slide range:     %7.3f %7.3f %7.3f\n" % self.slide_range
        retval += "number of transforms: %i" % self.getNumTransforms()
        return retval



class TransformationVectors:
    
    def __init__( self ):
        pass



class Configuration:
    
    
    def __init__( self, filename = None ):
        
        self.input_structure_filename = None
        self.output_basename = None
        self.transformation_residue_ranges = None
        self.transformation_generators = None
        if filename is not None:
            self.parseConfigFile( filename )


    def parseConfigFile( self, filename ):
        
        config_dictionary = {}
        
        with open( filename, "r" ) as infile:
            for line in infile:
                comment_match = re.match( "(.*)(#.*)", line )
                if comment_match:
                    line = comment_match.group(1)
                line = line.replace("=", " ")
                line_split = line.split()
                if len( line_split ) > 0:
                    line_type = line_split[0]
                    line_data = line_split[1:]
                    if line_type in config_dictionary:
                        print("Warning: found duplicate: ", line_type)
                    config_dictionary[ line_type ] = line_data
    
        transformation_configurations = list( six.moves.zip_longest( config_dictionary.get( "chains", [] ),
                                                                     tuplesFromList( config_dictionary.get( "residue_ranges", [] ), 2 ),
                                                                     tuplesFromList( config_dictionary.get( "tilt_ranges", [] ), 3 ),
                                                                     tuplesFromList( config_dictionary.get( "gyrate_ranges", [] ), 3 ),
                                                                     tuplesFromList( config_dictionary.get( "rotate_ranges", [] ), 3 ),
                                                                     tuplesFromList( config_dictionary.get( "translate_ranges", [] ), 3 ),
                                                                     tuplesFromList( config_dictionary.get( "elevate_ranges", [] ), 3 ),
                                                                     tuplesFromList( config_dictionary.get( "slide_ranges", [] ), 3 ) ) )
        
        self.transformation_residue_ranges = []
        self.transformation_generators = []
        for tc in transformation_configurations:
            self.transformation_residue_ranges.append( ( tc[0], ( int(tc[1][0]), int(tc[1][1]) ) ) )
            self.transformation_generators.append( TranformationGenerator( tc[0], tc[1], tc[2], tc[3], tc[4], tc[5], tc[6], tc[7] ) )

        self.transformation_generator = itertools.product( *( tg.product for tg in self.transformation_generators ) )

        self.input_structure_filename = config_dictionary.get( "input_structure_filename", [None] )[0]
        self.input_symm_filename = config_dictionary.get( "input_symm_filename", [None] )[0]
        self.input_mtz_filename = config_dictionary.get( "input_mtz_filename", [None] )[0]
        self.input_charmm_pdb = config_dictionary.get( "input_charmm_pdb", [None] )[0]
        self.input_charmm_psf = config_dictionary.get( "input_charmm_psf", [None] )[0]
        self.input_join_scwrl_tcl = config_dictionary.get( "input_join_scwrl_tcl", [None] )[0]

        self.output_basename = config_dictionary.get( "output_basename", ["liticon"] )[0]
        
        self.dump_pdbs = config_dictionary.get( "dump_pdbs", "False" )[0].lower() in [ "true" ]


    def getNumTransforms( self ):
        
        return reduce( operator.mul, [ x.getNumTransforms() for x in self.transformation_generators ], 1 )
    
    
    def __str__( self ):

        retval = "\n".join( [ str(x) for x in self.transformation_generators ] )
        retval += "\ntotal number of transformations: %i" % self.getNumTransforms()
        return retval



class Structure:
    
    
    def __init__( self, input_structure_filename ):

        self.input_structure_filename = input_structure_filename



class StructureRosetta( Structure ):
    
    
    def __init__( self,
                  input_structure_filename,
                  score_function_name = "beta",
                  soft_repacking = True,
                  mutation_repacking_radius = -1.0,
                  transform_repacking_radius = -1.0,
                  extra_params_filenames = [],
                  all_atom_positional_constraint_filename = None,
                  detect_disulf_tolerance = 3.0,
                  verbose = True):

        self.score_function_name = score_function_name
        
        self.using_mpframework = False
        if score_function_name[:11] == "mpframework":
            self.using_mpframework = True
        
        self.init_args = [ "Liticon-Rosetta", "-ignore_zero_occupancy", "false", "-in::detect_disulf_tolerance", str(detect_disulf_tolerance) ]
        
        if self.using_mpframework:
            self.init_args += [ "-mp::setup::spans_from_structure", "true" ]
        
        if extra_params_filenames is not None and len( extra_params_filenames ) > 0:
            print("extra_params_filenames", extra_params_filenames)
            self.init_args += [ "--extra_res_fa" ] + extra_params_filenames

        if score_function_name is "corrections_conway2016":
            self.init_args += [ "-dun10_dir", "rotamer/corrections_conway2016" ]

        if all_atom_positional_constraint_filename is not None:
            self.init_args += [ "-cst_fa_file", all_atom_positional_constraint_filename ]
        
        self.soft_init_args = copy.deepcopy( self.init_args )
        
        if score_function_name == "beta":
            self.init_args += [ "-beta -score::weights", "beta_cart.wts" ]
            self.soft_init_args += [ "-beta -score::weights", "beta_soft.wts" ]
        else:
            self.init_args += [ "-score::weights", score_function_name ]
            self.soft_init_args += [ "-score::weights", "soft_rep_design.wts" ]
    
        Rosetta.init( self.init_args )
        
        self.soft_repacking = soft_repacking
        self.mutation_repacking_radius = mutation_repacking_radius
        self.transform_repacking_radius = transform_repacking_radius
        
        
        Structure.__init__( self, input_structure_filename )
        self.working_cartesian_coords = None

        self.score_function = Rosetta.ScoreFunction()
        self.score_function.initialize_from_file( score_function_name )
        self.score_function.set_weight( Rosetta.ScoreType.cart_bonded, 1.0 )
        self.score_function.set_weight( Rosetta.ScoreType.pro_close, 0.0 )
        
        self.pose = poseFromPdbAutoTer( input_structure_filename )
        
        if self.using_mpframework:
            add_membrane_mover = Rosetta.AddMembraneMover()
            add_membrane_mover.apply( self.pose )
            membrane_position_from_topology_mover = Rosetta.MembranePositionFromTopologyMover()
            membrane_position_from_topology_mover.apply( self.pose )
                
            
        if all_atom_positional_constraint_filename is not None:
            Rosetta.addFaConstraintsFromCmdline( self.pose, self.score_function )

            
        self.cartesian_minimizer_map = Rosetta.CartesianMinimizerMap()
        
        self.move_map = Rosetta.MoveMap()
        self.move_map.set_bb( True )
        self.move_map.set_chi( True )
        
        self.reset()
        

    
    def _refreshOnChangeWorkingPose( self ):
 
        self.cartesian_minimizer_map.setup( self.working_pose, self.move_map )
 
        cartesian_coords_multivec = Rosetta.Multivec( self.cartesian_minimizer_map.ndofs() ) 
        self.cartesian_minimizer_map.copy_dofs_from_pose( self.working_pose, cartesian_coords_multivec ) 
        numpy_cartesian_coords = Rosetta.numpyArrayFromMultivec( cartesian_coords_multivec )
        self.working_cartesian_coords = numpy.reshape( numpy_cartesian_coords,
                                                       ( -1, 3 ) ) 
        num_residues = self.working_pose.total_residue()   
        self.pose_residue_cartesian_coords_indices = {} 
        self.pose_residue_cartesian_coords_atom_names = {}

        current_pose_residue_coords_base_index = 0

        for residue_id in range( 1, num_residues + 1 ):
            pose_residue = self.working_pose.residue( residue_id )
            num_atoms = pose_residue.natoms()
            #residue_type = pose_residue.type()
            residue_atom_indices = []
            residue_atom_names = []
            for atom_index in range( 0, num_atoms ):
                residue_atom_indices.append( atom_index )
                residue_atom_names.append( self.working_pose.residue_type( residue_id ).atom_name( atom_index + 1 ) )
            self.pose_residue_cartesian_coords_indices[ residue_id ] = [ ( current_pose_residue_coords_base_index + i ) for i in residue_atom_indices ]
            self.pose_residue_cartesian_coords_atom_names[ residue_id ] = residue_atom_names
            current_pose_residue_coords_base_index += num_atoms
        
        self.pdb_info = self.working_pose.pdb_info()


    def _refreshOnChangeWorkingCoords( self ):

        cartesian_coords_multivec = Rosetta.multivecFromNumpyArray( self.working_cartesian_coords )
        self.cartesian_minimizer_map.copy_dofs_to_pose( self.working_pose, cartesian_coords_multivec )


    def _getResidueCoordinatesIndices( self, chain_id, residue_id ):

        return self.pose_residue_cartesian_coords_indices[ self.pdb_info.pdb2pose( chain_id, residue_id, ' ', ' ' ) ]


    def _getResidueCoordinatesAtomNames( self, chain_id, residue_id ):

        return self.pose_residue_cartesian_coords_atom_names[ self.pdb_info.pdb2pose( chain_id, residue_id, ' ', ' ' ) ]
    

    def getTransformationVectors( self, configuration, selected_atom_names = [ "CA" ], old_style = False ):

        chain_ids = [ t[0] for t in configuration.transformation_residue_ranges ]
        residue_ranges = [ list(range( t[1][0], t[1][1] + 1)) for t in configuration.transformation_residue_ranges ]

        residue_range_cartesian_coordinate_sets = []
        
        for chain_id, residue_range in zip( chain_ids, residue_ranges ):
            
            if old_style:            
                residue_range_mid = int( residue_range[-1] + residue_range[0] ) / 2
                residue_range_lo = residue_range_mid - 7
                residue_range_hi = residue_range_mid + 7
                residue_range = list(range( residue_range_lo-1, residue_range_hi))
            
            residue_range_coordinates_indices = []
            for residue_id in residue_range:
                residue_coordinates_indices = self._getResidueCoordinatesIndices( chain_id, residue_id )
                residue_coordinates_atom_names = self._getResidueCoordinatesAtomNames( chain_id, residue_id )
                for i, coordinates_index in enumerate( residue_coordinates_indices ):
                    if residue_coordinates_atom_names[i].strip() in selected_atom_names:
                        residue_range_coordinates_indices.append( coordinates_index )                
            residue_range_cartesian_coordinates = numpy.empty( ( len(residue_range_coordinates_indices), 3 ) )
            for i, residue_coordinates_index in enumerate( residue_range_coordinates_indices ):
                residue_range_cartesian_coordinates[i] = self.working_cartesian_coords[residue_coordinates_index]
            residue_range_cartesian_coordinate_sets.append( residue_range_cartesian_coordinates )
            
        range_centroids = []
        range_up_vectors = []
        for residue_range_cartesian_coordinate_set in residue_range_cartesian_coordinate_sets:
            #print len( residue_range_cartesian_coordinate_set ), residue_range_cartesian_coordinate_set[:3]
            range_centroids.append( numpy.mean( residue_range_cartesian_coordinate_set, axis=0 ) )
        for residue_range_cartesian_coordinate_set in residue_range_cartesian_coordinate_sets:
            #range_up_vectors.append( numpy.mean(residue_range_cartesian_coordinate_set[:(len(residue_range_cartesian_coordinate_set)/2)], axis=0 ) -
            #                         numpy.mean(residue_range_cartesian_coordinate_set[(len(residue_range_cartesian_coordinate_set)/2):], axis=0 ) )
            #print calcPrincipalAxis(residue_range_cartesian_coordinate_set)
            #print calcPrincipalAxisOld(residue_range_cartesian_coordinate_set)
            range_up_vectors.append( calcPrincipalAxis(residue_range_cartesian_coordinate_set) )
        
        if len( range_centroids ) > 1:
            structure_centroid = numpy.mean( numpy.array( range_centroids ), axis=0 )
        else:
            structure_centroid = numpy.mean( self.working_cartesian_coords, axis=0 )
        
        # normalize
        normalized_range_up_vectors = []
        for range_up_vector in range_up_vectors:
            normalized_range_up_vectors.append( range_up_vector/numpy.linalg.norm(range_up_vector) )
        range_up_vectors = normalized_range_up_vectors
        
        # align in same direction
        aligned_range_up_vectors = [ range_up_vectors[0] ]
        for range_up_vector in range_up_vectors[1:]:
            if numpy.dot( aligned_range_up_vectors[0], range_up_vector ) < 0.0:
                aligned_range_up_vectors.append( -range_up_vector )
            else:
                aligned_range_up_vectors.append( range_up_vector )
        range_up_vectors = aligned_range_up_vectors
        structure_up_vector = numpy.mean( range_up_vectors, axis=0 )
        
        range_translate_vectors = [ ( rc - structure_centroid ) for rc in range_centroids ]
        
        normalized_range_translate_vectors = []
        for range_translate_vector in range_translate_vectors:
            range_translate_vector_norm = numpy.linalg.norm( range_translate_vector )
            if range_translate_vector_norm > 0.0:
                normalized_range_translate_vectors.append( range_translate_vector / range_translate_vector_norm )
            else:
                normalized_range_translate_vectors.append( range_translate_vector )
        range_translate_vectors = normalized_range_translate_vectors

        return structure_up_vector, range_up_vectors, range_translate_vectors, range_centroids
    
    
    def reset( self ):
        
        self.working_pose = self.pose.clone()
        
        self._refreshOnChangeWorkingPose()   
    
    def mutateResidue( self, resid, mutate_resname ):
        
        mutate_residue_mover = Rosetta.MutateResidueMover( resid, mutate_resname )
        mutate_residue_mover.apply( self.working_pose )
        self._refreshOnChangeWorkingPose()    
    

    def transformResidues( self, chain_id, residue_ids, transformation, structure_up_vector, range_up_vector, range_translate_vector, range_centroid ):
        
        tilt_amount = transformation[0]
        gyrate_amount = transformation[1]
        rotate_amount = transformation[2]
        translate_amount = transformation[3]
        elevate_amount = transformation[4]
        slide_amount = transformation[5]
        
        residue_coordinates_indices = []
        for residue_id in residue_ids:
            residue_coordinates_indices.extend( self._getResidueCoordinatesIndices( chain_id, residue_id ) )
                
        tilt_vector = numpy.cross( range_up_vector, range_translate_vector )
        tilt_matrix = getRotationMatrix( tilt_vector, math.radians( tilt_amount ) )
        range_up_vector = numpy.dot( tilt_matrix, range_up_vector )
        rotate_matrix = getRotationMatrix( range_up_vector, math.radians( rotate_amount ) )
        gyrate_matrix = getRotationMatrix( structure_up_vector, math.radians( gyrate_amount ) )
                
        for residue_coordinates_index in residue_coordinates_indices:
            self.working_cartesian_coords[residue_coordinates_index] -= range_centroid
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot(tilt_matrix, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot(rotate_matrix, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot(gyrate_matrix, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] += range_centroid
            
            self.working_cartesian_coords[residue_coordinates_index] = self.working_cartesian_coords[residue_coordinates_index] + \
                                                                       ( structure_up_vector * elevate_amount ) + \
                                                                       ( range_translate_vector * translate_amount ) + \
                                                                       ( tilt_vector * slide_amount ) 

        self._refreshOnChangeWorkingCoords()


    def transformResiduesRandom( self, chain_id, residue_ids, max_translate_amount ):
        
        residue_coordinates_indices = []
        for residue_id in residue_ids:
            residue_coordinates_indices.extend( self._getResidueCoordinatesIndices( chain_id, residue_id ) )

        range_centroid = numpy.zeros( [3] )
        for residue_coordinates_index in residue_coordinates_indices:
            range_centroid += self.working_cartesian_coords[residue_coordinates_index]
        range_centroid /= float( len( residue_coordinates_indices ) )
        
        random_rotation_matrix_1 = getRandomRotationMatrix()
        random_rotation_matrix_2 = getRandomRotationMatrix()
        random_rotation_matrix_3 = getRandomRotationMatrix()
        random_translate_vector = getRandomAxis() * numpy.random.random() * max_translate_amount
        for residue_coordinates_index in residue_coordinates_indices:
            self.working_cartesian_coords[residue_coordinates_index] -= range_centroid
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot(random_rotation_matrix_1, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot(random_rotation_matrix_2, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot(random_rotation_matrix_3, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] += range_centroid
            self.working_cartesian_coords[residue_coordinates_index] += random_translate_vector
            
        self._refreshOnChangeWorkingCoords()


    def repack( self,
                transform_chain_id = None,
                transform_residue_ids = None,
                mutation_chain_id = None,
                mutation_residue_ids = None,
                verbose = False ):
        # 
        # if ( transform_residue_ids is not None and len(transform_residue_ids) == 0 and self.transform_repacking_radius >= 0.0 ) and
        #    ( mutation_residue_ids is not None and len(mutation_residue_ids) == 0 and self.mutation_repacking_radius >= 0.0 )
        # if chain_id is None and residue_ids is not None:
        #     print( "ERROR: repack called with specific resnums but without a chain id" )
        #     exit()
        
        # repack
        if self.soft_repacking:
            Rosetta.init( self.soft_init_args )

        packing_score_function = Rosetta.ScoreFunction()
        if self.soft_repacking and self.score_function_name != "beta":
            packing_score_function.initialize_from_file( "soft_rep_design" )
        else:
            packing_score_function.initialize_from_file( self.score_function_name )
        packing_score_function.set_weight( Rosetta.ScoreType.cart_bonded, 1.0 )
        packing_score_function.set_weight( Rosetta.ScoreType.pro_close, 0.0 )
    
        packing_score_function( self.working_pose )
        task_factory = Rosetta.TaskFactory()
        packer_task = task_factory.create_task_and_apply_taskoperations( self.working_pose )
        packer_task.restrict_to_repacking()
        packer_task.or_include_current(True)
        
        repack_map = None
        
        if transform_residue_ids is not None and self.transform_repacking_radius >= 0.0:
            
            repack_map = Rosetta.getEmptyNeighborResnumsMap( self.working_pose )
            
            for residue_id in transform_residue_ids:
                this_repack_map = Rosetta.getNeighborResnumsMap( self.working_pose,
                                                                 self.pdb_info.pdb2pose( transform_chain_id, residue_id, ' ', ' ' ),
                                                                 self.transform_repacking_radius )
                if repack_map is None:
                    repack_map = this_repack_map
                else:
                    repack_map = Rosetta.orNeighborResnumsMaps( repack_map, this_repack_map )
        
            Rosetta.setRepackingResnums( self.working_pose,
                                         packer_task,
                                         repack_map )
        
        if mutation_residue_ids is not None and self.mutation_repacking_radius >= 0.0:
            
            if repack_map is None:
                repack_map = Rosetta.getEmptyNeighborResnumsMap( self.working_pose )
                
            for residue_id in mutation_residue_ids:
                this_repack_map = Rosetta.getNeighborResnumsMap( self.working_pose,
                                                                 self.pdb_info.pdb2pose( mutation_chain_id, residue_id, ' ', ' ' ),
                                                                 self.mutation_repacking_radius )
                if repack_map is None:
                    repack_map = this_repack_map
                else:
                    repack_map = Rosetta.orNeighborResnumsMaps( repack_map, this_repack_map )

        if repack_map is not None:
            Rosetta.setRepackingResnums( self.working_pose,
                                         packer_task,
                                         repack_map )

        pack_rotamers_mover = Rosetta.PackRotamersMover( packing_score_function, packer_task )
        
        pack_rotamers_mover.apply( self.working_pose )

        if self.soft_repacking:
            Rosetta.init( self.init_args )

        self._refreshOnChangeWorkingPose()


    def getNumClashes( self,
                       clash_distance = 0.5,
                       protein_chain_id = None,
                       ligand_chain_id = None,
                       protein_atom_names = [ "C", "CA", "N", "O", "CB" ] ):
        print( "Clashes not implemented yet.." )
        exit()
        # print( set( 
        # if protein_chain_id is not None and ligand_chain_id is not None:
        #     protein_cartesian_coords_indices = []
        # 
        # 
        # else:
        #     distances = numpy.ndarray.flatten( numpy.triu( scipy.spatial.distance.cdist( self.working_cartesian_coords, self.working_cartesian_coords ) ) )
        #     clash_distances = distances[ numpy.nonzero( distances ) ]
        #     clash_distances = clash_distances[ numpy.where( clash_distances < clash_distance ) ]
        #     print( clash_distances )
        #     return len( clash_distances )


    def minimize( self, method = "lbfgs_armijo_nonmonotone", threshold = 0.001 ):

        minimizer_options = Rosetta.MinimizerOptions( method, threshold, True )

        move_map = Rosetta.MoveMap()
        move_map.set_bb( True )
        move_map.set_chi( True )
        
        cartesian_minimizer = Rosetta.CartesianMinimizer()
        
        # print "Score before minimization: %7.3f" % ( self.score_function( self.working_pose ), )
    
        self.cartesian_minimizer_map.setup( self.working_pose, move_map )
        
        self.score_function.setup_for_minimizing( self.working_pose, self.cartesian_minimizer_map )
        
        cartesian_minimizer.run( self.working_pose, move_map, self.score_function, minimizer_options )
        
        # print "Score after linear minimization: %7.3f" % ( self.score_function( self.working_pose ), )
    
        self._refreshOnChangeWorkingPose()
        
        
    def getScore( self ):
        
        return self.score_function( self.working_pose )
        
        
    def dumpScore( self, output_filename ):
        
        Rosetta.show( self.score_function, self.working_pose, output_filename )
        
        
    def dump_pdb( self, output_pdb_filename ):
        
        self.working_pose.dump_pdb( output_pdb_filename )


 
class StructureRosettaPhenix( StructureRosetta ):


    def __init__( self,
                  input_structure_filename,
                  input_symm_filename,
                  input_mtz_filename,
                  score_function_name = "ref2015" ):
        
        self.input_symm_filename = input_symm_filename
        self.input_mtz_filename = input_mtz_filename
        
        Rosetta.init( [ "Rosetta", "-cryst::mtzfile", self.input_mtz_filename, "-cryst::crystal_refine" ] )
        
        self.phenix_interface = Rosetta.getPhenixInterface()

        self.symmetric_score_function_xtal = Rosetta.SymmetricScoreFunction()
        self.symmetric_score_function_xtal.initialize_from_file( score_function_name )
        self.symmetric_score_function_xtal.set_weight( Rosetta.ScoreType.cart_bonded, 1.0 )
        self.symmetric_score_function_xtal.set_weight( Rosetta.ScoreType.pro_close, 0.0 )
        
        self.symmetric_score_function_dens = Rosetta.SymmetricScoreFunction()
        self.symmetric_score_function_dens.initialize_from_file( score_function_name )
        self.symmetric_score_function_dens.set_weight( Rosetta.ScoreType.cart_bonded, 1.0 )
        self.symmetric_score_function_dens.set_weight( Rosetta.ScoreType.pro_close, 0.0 )
        self.symmetric_score_function_dens.set_weight( Rosetta.ScoreType.elec_dens_fast, 20.0 )
        self.symmetric_score_function_dens.set_weight( Rosetta.ScoreType.xtal_ml, 0.0 )
        
        self.set_refinement_options_mover = Rosetta.SetRefinementOptionsMover()
        self.set_refinement_options_mover.setMapType( "2mFo-DFc" )
        
        self.setup_for_symmetry_mover = Rosetta.SetupForSymmetryMover( input_symm_filename )
    
        self.extract_asymmetric_unit_mover = Rosetta.ExtractAsymmetricUnitMover()
        
        self.recompute_density_map_mover = Rosetta.RecomputeDensityMapMover()
    
        self.fit_b_factors_mover = Rosetta.FitBfactorsMover()
        self.fit_b_factors_mover.setAdpStrategy("individual")
    
        self.set_cryst_weight_cartesian_mover = Rosetta.SetCrystWeightMover()
        self.set_cryst_weight_cartesian_mover.set_weightScale( 0.5 )
        self.set_cryst_weight_cartesian_mover.setScoreFunction( self.symmetric_score_function_xtal )
        self.set_cryst_weight_cartesian_mover.setScoreFunctionRef( self.symmetric_score_function_xtal )
        self.set_cryst_weight_cartesian_mover.setCartesian( True )
        self.set_cryst_weight_cartesian_mover.set_bb( True )
        self.set_cryst_weight_cartesian_mover.set_chi( True )
        
        self.task_factory = Rosetta.TaskFactory()
        #sym_pack_rotamers_mover_dens_soft = SymPackRotamersMover( symmetric_score_function_dens_soft, packer_task )
        
        #sym_min_mover_tors_xtal = SymMinMover( symmetric_move_map, symmetric_score_function_combo, "lbfgs_armijo_rescored", 0.0001, True )
        #sym_min_mover_dens = SymMinMover( symmetric_move_map, symmetric_score_function_dens, "lbfgs_armijo", 0.00001, True )
        
        StructureRosetta.__init__( self, input_structure_filename, score_function_name )
        
        
    def reset( self ):

        StructureRosetta.reset( self )
        
        self.working_symmetric_pose = self.pose.clone()
        self.set_refinement_options_mover.apply( self.working_symmetric_pose )
        
        self.setup_for_symmetry_mover.apply( self.working_symmetric_pose )
        
        self.symmetric_move_map = Rosetta.MoveMap()
        self.symmetric_move_map.set_bb( True )
        self.symmetric_move_map.set_chi( True )
        Rosetta.makeSymmetricMovemap( self.working_symmetric_pose, self.symmetric_move_map )
        
        self.packer_task = self.task_factory.create_task_and_apply_taskoperations( self.working_symmetric_pose )
        self.packer_task.restrict_to_repacking()
        self.packer_task.orIncludeCurrent( True )
        self.extra_rotamers_generic = Rosetta.ExtraRotamersGeneric()
        self.sym_pack_rotamers_mover_dens = Rosetta.SymPackRotamersMover( self.symmetric_score_function_dens, self.packer_task )
        self.sym_min_mover_cart_xtal = Rosetta.SymMinMover( self.symmetric_move_map, self.symmetric_score_function_xtal, "lbfgs_armijo_rescored", 0.0001, True )
        
        self.cartesian_minimizer_map = Rosetta.CartesianMinimizerMap()
        self.cartesian_minimizer_map.setup( self.working_symmetric_pose, self.symmetric_move_map )
        
        self.recompute_density_map_mover.apply( self.working_symmetric_pose )
        self.fit_b_factors_mover.apply( self.working_symmetric_pose )
        
        
    def _refreshOnChangeWorkingCoords( self ):

        cartesian_coords_multivec = Rosetta.multivecFromNumpyArray( self.working_cartesian_coords )
        self.cartesian_minimizer_map.copy_dofs_to_pose( self.working_symmetric_pose, cartesian_coords_multivec )
        
        self.symmetric_score_function_xtal( self.working_symmetric_pose )
        
        self.recompute_density_map_mover.apply( self.working_symmetric_pose )
        
        self.fit_b_factors_mover.apply( self.working_symmetric_pose )
        
        self.symmetric_score_function_xtal( self.working_symmetric_pose )
        
        StructureRosetta._refreshOnChangeWorkingCoords( self )
        
        
    def _refreshOnChangeWorkingPose( self ):
        
        StructureRosetta._refreshOnChangeWorkingPose( self )
        self.working_symmetric_pose = self.working_pose.clone()
        self.setup_for_symmetry_mover.apply( self.working_symmetric_pose )

        self.symmetric_move_map = Rosetta.MoveMap()
        self.symmetric_move_map.set_bb( True )
        self.symmetric_move_map.set_chi( True )
        Rosetta.makeSymmetricMovemap( self.working_symmetric_pose, self.symmetric_move_map )
        
        self.cartesian_minimizer_map = Rosetta.CartesianMinimizerMap()
        self.cartesian_minimizer_map.setup( self.working_symmetric_pose, self.symmetric_move_map )
        self.symmetric_score_function_xtal( self.working_symmetric_pose )
        
        self.recompute_density_map_mover.apply( self.working_symmetric_pose )
        
        self.fit_b_factors_mover.apply( self.working_symmetric_pose )
        
        self.symmetric_score_function_xtal( self.working_symmetric_pose )


    def _refreshOnChangeWorkingSymmetricPose( self ):
        
        self.working_pose = self.working_symmetric_pose.clone()
        self.extract_asymmetric_unit_mover.apply( self.working_pose )
        StructureRosetta._refreshOnChangeWorkingPose( self )


    def minimizeXtalReciprocalSpace( self,
                                     method = "lbfgs_armijo_nonmonotone" ):

        self.symmetric_move_map = Rosetta.MoveMap()
        self.symmetric_move_map.set_bb( True )
        self.symmetric_move_map.set_chi( True )
        Rosetta.makeSymmetricMovemap( self.working_symmetric_pose, self.symmetric_move_map )
        
        self.cartesian_minimizer_map = Rosetta.CartesianMinimizerMap()
        self.cartesian_minimizer_map.setup( self.working_symmetric_pose, self.symmetric_move_map )
        
        self.recompute_density_map_mover.apply( self.working_symmetric_pose )
        
        self.symmetric_score_function_xtal( self.working_symmetric_pose )
        
        self.set_cryst_weight_cartesian_mover.apply( self.working_symmetric_pose )
        
        self.symmetric_score_function_xtal.setup_for_minimizing( self.working_symmetric_pose, self.cartesian_minimizer_map )
        
        self.sym_min_mover_cart_xtal = Rosetta.SymMinMover( self.symmetric_move_map, self.symmetric_score_function_xtal, method, 0.0001, True )
        self.sym_min_mover_cart_xtal.setCartesian( True )
        self.sym_min_mover_cart_xtal.setMaxIter( 100 )
        self.sym_min_mover_cart_xtal.set_bb( True )
        self.sym_min_mover_cart_xtal.set_chi( True )
        self.sym_min_mover_cart_xtal.apply( self.working_symmetric_pose )
        
        self.fit_b_factors_mover.apply( self.working_symmetric_pose )
        
        # asym_unit_pose = pose.clone()
        # extract_asymmetric_unit_mover.apply( asym_unit_pose )
        # 
        # asym_unit_pose.dump_pdb( refinement_input_pdb_filename )
        # 
        # asym_unit_pose.dump_pdb( refined_pdb_filename )
        # 
        # cycle_id = "%04i_2_%04i_1" % ( macro_cycle, stage_3_cycle )
        # 
        # with open( refined_log_filename, "a" ) as outfile:
        #     outfile.write( "%s; r_work: %6.5f; r_free: %6.5f\n" % ( cycle_id, phenix_interface.getR(), phenix_interface.getRfree() ) )
        # 
        # asym_unit_pose.dump_pdb( basename + ".inter.%s.pdb" % ( cycle_id, ) )

        self._refreshOnChangeWorkingSymmetricPose()


    def minimizeXtalRealSpace( self,
                               method = "lbfgs_armijo_nonmonotone" ):

        self.symmetric_move_map = Rosetta.MoveMap()
        self.symmetric_move_map.set_bb( True )
        self.symmetric_move_map.set_chi( True )
        Rosetta.makeSymmetricMovemap( self.working_symmetric_pose, self.symmetric_move_map )
        
        self.cartesian_minimizer_map = Rosetta.CartesianMinimizerMap()
        self.cartesian_minimizer_map.setup( self.working_symmetric_pose, self.symmetric_move_map )
        
        self.recompute_density_map_mover.apply( self.working_symmetric_pose )
        
        self.symmetric_score_function_dens( self.working_symmetric_pose )
        
        self.set_cryst_weight_cartesian_mover.apply( self.working_symmetric_pose )
        
        self.symmetric_score_function_dens.setup_for_minimizing( self.working_symmetric_pose, self.cartesian_minimizer_map )
        
        self.sym_min_mover_cart_dens = Rosetta.SymMinMover( self.symmetric_move_map, self.symmetric_score_function_dens, method, 0.0001, True )
        self.sym_min_mover_cart_dens.setCartesian( True )
        self.sym_min_mover_cart_dens.setMaxIter( 100 )
        self.sym_min_mover_cart_dens.set_bb( True )
        self.sym_min_mover_cart_dens.set_chi( True )
        self.sym_min_mover_cart_dens.apply( self.working_symmetric_pose )
        
        self.fit_b_factors_mover.apply( self.working_symmetric_pose )
        
        # asym_unit_pose = pose.clone()
        # extract_asymmetric_unit_mover.apply( asym_unit_pose )
        # 
        # asym_unit_pose.dump_pdb( refinement_input_pdb_filename )
        # 
        # asym_unit_pose.dump_pdb( refined_pdb_filename )
        # 
        # cycle_id = "%04i_2_%04i_1" % ( macro_cycle, stage_3_cycle )
        # 
        # with open( refined_log_filename, "a" ) as outfile:
        #     outfile.write( "%s; r_work: %6.5f; r_free: %6.5f\n" % ( cycle_id, phenix_interface.getR(), phenix_interface.getRfree() ) )
        # 
        # asym_unit_pose.dump_pdb( basename + ".inter.%s.pdb" % ( cycle_id, ) )

        self._refreshOnChangeWorkingSymmetricPose()
        

    def getRWork( self ):
    
        return self.phenix_interface.getR()
    
    
    def getRFree( self ):
    
        return self.phenix_interface.getRfree()
    
    
class StructureSimple( Structure ):
    
    def __init__( self,
                  input_structure_filename,
                  verbose = True ):

        Structure.__init__( self, input_structure_filename )
        
        self.pose = Unimol.Universe( input_structure_filename,
                                     input_structure_filename )
        self._refreshOnChangeWorkingPose()   
    
    
    def _refreshOnChangeWorkingPose( self ):

        # cartesian_coords_multivec = Rosetta.Multivec( self.cartesian_minimizer_map.ndofs() ) 
        # self.cartesian_minimizer_map.copyDofsFromPose( self.working_pose, cartesian_coords_multivec ) 
        # numpy_cartesian_coords = Rosetta.numpyArrayFromMultivec( cartesian_coords_multivec )
        num_atoms = self.pose.getNumAtoms()
        
        self.working_cartesian_coords = self.pose.getCoordinates()[0]
        print( self.working_cartesian_coords )
        
        # num_residues = self.working_pose.total_residue()   
        # self.pose_residue_cartesian_coords_indices = {} 
        # self.pose_residue_cartesian_coords_atom_names = {}
        # 
        # current_pose_residue_coords_base_index = 0
        # 
        # for residue_id in range( 1, num_residues + 1 ):
        #     pose_residue = self.working_pose.residue( residue_id )
        #     num_atoms = pose_residue.natoms()
        #     residue_type = pose_residue.type()
        #     residue_atom_indices = []
        #     residue_atom_names = []
        #     for atom_index in range( 0, num_atoms ):
        #         residue_atom_indices.append( atom_index )
        #         residue_atom_names.append( self.working_pose.residueType( residue_id ).atomName( atom_index + 1 ) )
        #     self.pose_residue_cartesian_coords_indices[ residue_id ] = [ ( current_pose_residue_coords_base_index + i ) for i in residue_atom_indices ]
        #     self.pose_residue_cartesian_coords_atom_names[ residue_id ] = residue_atom_names
        #     current_pose_residue_coords_base_index += num_atoms
        # 
        # self.pdb_info = self.working_pose.pdb_info()


    def _refreshOnChangeWorkingCoords( self ):
        pass


    def _getResidueCoordinatesIndices( self, chain_id, residue_id ):
        return self.pose.getAtomIndicesByResidueIdRange( residue_id, residue_id, chain_id )


    def _getResidueCoordinatesAtomNames( self, chain_id, residue_id ):
        pass


    def getTransformationVectors( self,
                                  chain_ids,
                                  residue_ranges,
                                  selected_atom_names = [ "CA" ],
                                  old_style = False ):
        print( "chain_ids", chain_ids )
        print( "residue_ranges", residue_ranges )
        residue_range_cartesian_coordinate_sets = []
        
        for chain_id, residue_range in zip( chain_ids, residue_ranges ):
            
            if old_style:
                residue_range_mid = int( residue_range[-1] + residue_range[0] ) / 2
                residue_range_lo = residue_range_mid - 7
                residue_range_hi = residue_range_mid + 7
                residue_range = list(range( residue_range_lo-1, residue_range_hi ))
            
            residue_range_coordinates_indices = []
            for residue_id in residue_range:
                residue_coordinates_indices = self._getResidueCoordinatesIndices( chain_id, residue_id )
                print( chain_id, residue_id, residue_coordinates_indices )
                residue_coordinates_atom_names = self._getResidueCoordinatesAtomNames( chain_id, residue_id )
                for coordinates_index in residue_coordinates_indices:
                    residue_range_coordinates_indices.append( coordinates_index )                
            residue_range_cartesian_coordinates = numpy.empty( ( len(residue_range_coordinates_indices), 3 ) )
            for i, residue_coordinates_index in enumerate( residue_range_coordinates_indices ):
                residue_range_cartesian_coordinates[i-1] = self.working_cartesian_coords[residue_coordinates_index]
            residue_range_cartesian_coordinate_sets.append( residue_range_cartesian_coordinates )
            
        range_centroids = []
        range_up_vectors = []
        for residue_range_cartesian_coordinate_set in residue_range_cartesian_coordinate_sets:
            #print len( residue_range_cartesian_coordinate_set ), residue_range_cartesian_coordinate_set[:3]
            range_centroids.append( numpy.mean( residue_range_cartesian_coordinate_set, axis=0 ) )
        for residue_range_cartesian_coordinate_set in residue_range_cartesian_coordinate_sets:
            #range_up_vectors.append( numpy.mean(residue_range_cartesian_coordinate_set[:(len(residue_range_cartesian_coordinate_set)/2)], axis=0 ) -
            #                         numpy.mean(residue_range_cartesian_coordinate_set[(len(residue_range_cartesian_coordinate_set)/2):], axis=0 ) )
            #print calcPrincipalAxis(residue_range_cartesian_coordinate_set)
            #print calcPrincipalAxisOld(residue_range_cartesian_coordinate_set)
            range_up_vectors.append( calcPrincipalAxis(residue_range_cartesian_coordinate_set) )
        
        if len( range_centroids ) > 1:
            structure_centroid = numpy.mean( numpy.array( range_centroids ), axis=0 )
        else:
            structure_centroid = numpy.mean( self.working_cartesian_coords, axis=0 )
        
        # normalize
        normalized_range_up_vectors = []
        for range_up_vector in range_up_vectors:
            normalized_range_up_vectors.append( range_up_vector/numpy.linalg.norm(range_up_vector) )
        range_up_vectors = normalized_range_up_vectors
        
        # align in same direction
        aligned_range_up_vectors = [ range_up_vectors[0] ]
        for range_up_vector in range_up_vectors[1:]:
            if numpy.dot( aligned_range_up_vectors[0], range_up_vector ) < 0.0:
                aligned_range_up_vectors.append( -range_up_vector )
            else:
                aligned_range_up_vectors.append( range_up_vector )
        range_up_vectors = aligned_range_up_vectors
        structure_up_vector = numpy.mean( range_up_vectors, axis=0 )
        
        range_translate_vectors = [ ( rc - structure_centroid ) for rc in range_centroids ]
        
        normalized_range_translate_vectors = []
        for range_translate_vector in range_translate_vectors:
            range_translate_vector_norm = numpy.linalg.norm( range_translate_vector )
            if range_translate_vector_norm > 0.0:
                normalized_range_translate_vectors.append( range_translate_vector / range_translate_vector_norm)
            else:
                normalized_range_translate_vectors.append( range_translate_vector )
        range_translate_vectors = normalized_range_translate_vectors

        return structure_up_vector, range_up_vectors, range_translate_vectors, range_centroids
    
    
    def reset( self ):

        self.pose = Unimol.Universe( self.input_structure_filename,
                                     self.input_structure_filename )
        
        self.working_cartesian_coords = self.pose.getCoordinates()[0]

    
    def mutateResidue( self, resid, mutate_resname ):
        pass 
    

    def transformResidues( self, chain_id, residue_ids, transformation, structure_up_vector, range_up_vector, range_translate_vector, range_centroid ):
        
        tilt_amount = transformation[0]
        gyrate_amount = transformation[1]
        rotate_amount = transformation[2]
        translate_amount = transformation[3]
        elevate_amount = transformation[4]
        slide_amount = transformation[5]
        
        residue_coordinates_indices = []
        for residue_id in residue_ids:
            residue_coordinates_indices.extend( self._getResidueCoordinatesIndices( chain_id, residue_id ) )
                
        tilt_vector = numpy.cross( range_up_vector, range_translate_vector )
        tilt_matrix = getRotationMatrix( tilt_vector, math.radians( tilt_amount ) )
        range_up_vector = numpy.dot( tilt_matrix, range_up_vector )
        rotate_matrix = getRotationMatrix( range_up_vector, math.radians( rotate_amount ) )
        gyrate_matrix = getRotationMatrix( structure_up_vector, math.radians( gyrate_amount ) )
                
        for residue_coordinates_index in residue_coordinates_indices:
            self.working_cartesian_coords[residue_coordinates_index] -= range_centroid
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot(tilt_matrix, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot(rotate_matrix, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot(gyrate_matrix, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] += range_centroid
            
            self.working_cartesian_coords[residue_coordinates_index] = self.working_cartesian_coords[residue_coordinates_index] + \
                                                                       ( structure_up_vector * elevate_amount ) + \
                                                                       ( range_translate_vector * translate_amount ) + \
                                                                       ( tilt_vector * slide_amount ) 

        self._refreshOnChangeWorkingCoords()


    def transformResiduesRandom( self, chain_id, residue_ids, max_translate_amount ):
        
        residue_coordinates_indices = []
        for residue_id in residue_ids:
            residue_coordinates_indices.extend( self._getResidueCoordinatesIndices( chain_id, residue_id ) )

        range_centroid = numpy.zeros( [3] )
        for residue_coordinates_index in residue_coordinates_indices:
            range_centroid += self.working_cartesian_coords[residue_coordinates_index]
        range_centroid /= float( len( residue_coordinates_indices ) )
        
        random_rotation_matrix_1 = getRandomRotationMatrix()
        random_rotation_matrix_2 = getRandomRotationMatrix()
        random_rotation_matrix_3 = getRandomRotationMatrix()
        random_translate_vector = getRandomAxis() * numpy.random.random() * max_translate_amount
        for residue_coordinates_index in residue_coordinates_indices:
            self.working_cartesian_coords[residue_coordinates_index] -= range_centroid
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot( random_rotation_matrix_1, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot( random_rotation_matrix_2, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] = numpy.dot( random_rotation_matrix_3, self.working_cartesian_coords[residue_coordinates_index] )
            self.working_cartesian_coords[residue_coordinates_index] += range_centroid
            self.working_cartesian_coords[residue_coordinates_index] += random_translate_vector
            
        self._refreshOnChangeWorkingCoords()


    def repack( self ):
        pass


    def getNumClashes( self ):
        pass


    def minimize( self, method = "lbfgs_armijo_nonmonotone" ):
        pass
    
    def dump_pdb( self, output_pdb_filename ):
        self.pose.writePDB( output_pdb_filename, self.working_cartesian_coords )


